package io.calumg.sudoku.solver;

import io.calumg.sudoku.board.NineByNineSudokuBoard;
import io.calumg.sudoku.board.SudokuBoard;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by calum on 09/05/2015.
 */
public class NineByNineSudokuSolverTest {

    private SudokuBoard unsolved;
    private SudokuBoard correctlySolved;

    @Before
    public void setup() {
        unsolved = new NineByNineSudokuBoard() {
            @Override
            public Integer[][] getCells() {
                return new Integer[][]{
                        {5, 3, null, null, 7, null, null, null, null},
                        {6, null, null, 1, 9, 5, null, null, null},
                        {null, 9, 8, null, null, null, null, 6, null},
                        {8, null, null, null, 6, null, null, null, 3},
                        {4, null, null, 8, null, 3, null, null, 1},
                        {7, null, null, null, 2, null, null, null, 6},
                        {null, 6, null, null, null, null, 2, 8, null},
                        {null, null, null, 4, 1, 9, null, null, 5},
                        {null, null, null, null, 8, null, null, 7, 9}
                };
            }
        };
        correctlySolved = new NineByNineSudokuBoard() {
            @Override
            public Integer[][] getCells() {
                return new Integer[][]{
                        {5, 3, 4, 6, 7, 8, 9, 1, 2},
                        {6, 7, 2, 1, 9, 5, 3, 4, 8},
                        {1, 9, 8, 3, 4, 2, 5, 6, 7},
                        {8, 5, 9, 7, 6, 1, 4, 2, 3},
                        {4, 2, 6, 8, 5, 3, 7, 9, 1},
                        {7, 1, 3, 9, 2, 4, 8, 5, 6},
                        {9, 6, 1, 5, 3, 7, 2, 8, 4},
                        {2, 8, 7, 4, 1, 9, 6, 3, 5},
                        {3, 4, 5, 2, 8, 6, 1, 7, 9}
                };
            }
        };
    }

    @Test
    public void testSolve() throws Exception {
        NineByNineSudokuSolver solver = new NineByNineSudokuSolver(unsolved);
        solver.solve();
        Integer[][] cells = solver.getSolvedBoard().getCells();
        for (int row = 0; row < cells.length; row++) {
            for (int column = 0; column < cells[0].length; column++) {
                assertEquals(correctlySolved.getCells()[row][column], cells[row][column]);
            }
        }
    }
}
