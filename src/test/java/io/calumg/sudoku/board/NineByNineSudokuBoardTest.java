package io.calumg.sudoku.board;

import org.junit.Before;
import org.junit.Test;

import java.security.InvalidParameterException;

import static junit.framework.TestCase.*;

/**
 * Created by calum on 09/05/2015.
 */
public class NineByNineSudokuBoardTest {

    private NineByNineSudokuBoard board;

    @Before
    public void setup() {
        board = new NineByNineSudokuBoard();

    }

    @Test
    public void testSetCell() throws Exception {
        this.board.setCell(2, 3, 1);
        assertEquals(new Integer(1), this.board.getCells()[2][3]);
    }

    @Test(expected = InvalidParameterException.class)
    public void testFailsNegativeValues() throws Exception {
        this.board.setCell(2, 2, -1);
    }

    @Test(expected = InvalidParameterException.class)
    public void testFailsInvalidValues() throws Exception {
        this.board.setCell(2, 2, 10);
    }

    @Test
    public void testSetCells() throws Exception {
        Integer[][] cells = this.board.getCells().clone();
        cells [1][1] = 2;
        this.board.setCells(cells);
        assertEquals(new Integer(2), this.board.getCells()[1][1]);
    }

    @Test(expected = InvalidParameterException.class)
    public void testFailsSetCells() throws Exception {
        this.board.setCells(new Integer[2][2]);
    }
}
