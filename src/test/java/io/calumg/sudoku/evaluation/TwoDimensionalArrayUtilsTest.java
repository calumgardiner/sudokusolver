package io.calumg.sudoku.evaluation;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

/**
 * Created by calum on 09/05/2015.
 */
public class TwoDimensionalArrayUtilsTest {

    private Integer[][] cells;

    @Before
    public void setup() {
        cells = new Integer[3][3];
        // fill each cell with incremental integer
        // 1 2 3
        // 4 5 6
        // 7 8 9
        Integer i = 0;
        for (int row = 0; row < cells.length; row++) {
            for (int column = 0; column < cells[0].length; column++) {
                cells[row][column] = ++i;
            }
        }
    }


    @Test
    public void testSumOfRow() throws Exception {
        String msg = "Didn't sum the rows right!";
        assertTrue(msg, TwoDimensionalArrayUtils.sumOfRow(cells, 0) == (1 + 2 + 3));
        assertTrue(msg, TwoDimensionalArrayUtils.sumOfRow(cells, 1) == (4 + 5 + 6));
        assertTrue(msg, TwoDimensionalArrayUtils.sumOfRow(cells, 2) == (7 + 8 + 9));
    }

    @Test
    public void testSumOfColumn() throws Exception {
        String msg = "Didn't sum the columns right!";
        assertTrue(msg, TwoDimensionalArrayUtils.sumOfColumn(cells, 0) == (1 + 4 + 7));
        assertTrue(msg, TwoDimensionalArrayUtils.sumOfColumn(cells, 1) == (2 + 5 + 8));
        assertTrue(msg, TwoDimensionalArrayUtils.sumOfColumn(cells, 2) == (3 + 6 + 9));
    }

    @Test
    public void testRowContainsDuplicates() throws Exception {
        assertFalse(TwoDimensionalArrayUtils.rowContainsDuplicates(cells, 0));
        assertFalse(TwoDimensionalArrayUtils.rowContainsDuplicates(cells, 1));
        assertFalse(TwoDimensionalArrayUtils.rowContainsDuplicates(cells, 2));
        // add a duplicate and check it flags
        cells[0][1] = 1;
        assertTrue(TwoDimensionalArrayUtils.rowContainsDuplicates(cells, 0));
    }

    @Test
    public void testColumnContainsDuplicates() throws Exception {
        assertFalse(TwoDimensionalArrayUtils.columnContainsDuplicates(cells, 0));
        assertFalse(TwoDimensionalArrayUtils.columnContainsDuplicates(cells, 1));
        assertFalse(TwoDimensionalArrayUtils.columnContainsDuplicates(cells, 2));
        // add a duplicate and check it flags
        cells[1][0] = 1;
        assertTrue(TwoDimensionalArrayUtils.columnContainsDuplicates(cells, 0));
    }

    @Test
    public void testGetSubsetOfArray() throws Exception {
        List<Integer> subset = TwoDimensionalArrayUtils.getSubsetOfArray(cells, 0, 0, 2, 2);
        assertTrue(subset.remove(new Integer(1)));
        assertTrue(subset.remove(new Integer(2)));
        assertTrue(subset.remove(new Integer(4)));
        assertTrue(subset.remove(new Integer(5)));
        assertTrue(subset.isEmpty());
    }

    @Test
    public void testGetAllNonNullElementsInRow() throws Exception {
        cells[0][0] = null;
        List<Integer> nonNull = TwoDimensionalArrayUtils.getNonNullObjectsInRow(cells, 0);
        assertTrue(nonNull.size() == 2);
        assertTrue(nonNull.remove(new Integer(2)));
        assertTrue(nonNull.remove(new Integer(3)));
    }

    @Test
    public void testGetAllNonNullElementsInColumn() throws Exception {
        cells[0][0] = null;
        List<Integer> nonNull = TwoDimensionalArrayUtils.getNonNullObjectsInColumn(cells, 0);
        assertTrue(nonNull.size() == 2);
        assertTrue(nonNull.remove(new Integer(4)));
        assertTrue(nonNull.remove(new Integer(7)));
    }

}
