package io.calumg.sudoku.evaluation;

import io.calumg.sudoku.board.NineByNineSudokuBoard;
import io.calumg.sudoku.board.SudokuBoard;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

/**
 * Created by calum on 09/05/2015.
 */
public class NineByNineEvaluatorTest {

    private NineByNineEvaluator evaluator;
    private SudokuBoard correctTestBoard;
    private SudokuBoard incorrectTestBoard;

    @Before
    public void setup() {
        this.evaluator = new NineByNineEvaluator();
        // Override the cells, setting each one would be rather tiresome
        this.correctTestBoard = new NineByNineSudokuBoard() {
            @Override
            public Integer[][] getCells() {
                return new Integer[][]{
                        {5, 3, 4, 6, 7, 8, 9, 1, 2},
                        {6, 7, 2, 1, 9, 5, 3, 4, 8},
                        {1, 9, 8, 3, 4, 2, 5, 6, 7},
                        {8, 5, 9, 7, 6, 1, 4, 2, 3},
                        {4, 2, 6, 8, 5, 3, 7, 9, 1},
                        {7, 1, 3, 9, 2, 4, 8, 5, 6},
                        {9, 6, 1, 5, 3, 7, 2, 8, 4},
                        {2, 8, 7, 4, 1, 9, 6, 3, 5},
                        {3, 4, 5, 2, 8, 6, 1, 7, 9}
                };
            }
        };
        // As above but I added an incorrect value to the bottom right cell
        this.incorrectTestBoard = new NineByNineSudokuBoard() {
            @Override
            public Integer[][] getCells() {
                return new Integer[][]{
                        {5, 3, 4, 6, 7, 8, 9, 1, 2},
                        {6, 7, 2, 1, 9, 5, 3, 4, 8},
                        {1, 9, 8, 3, 4, 2, 5, 6, 7},
                        {8, 5, 9, 7, 6, 1, 4, 2, 3},
                        {4, 2, 6, 8, 5, 3, 7, 9, 1},
                        {7, 1, 3, 9, 2, 4, 8, 5, 6},
                        {9, 6, 1, 5, 3, 7, 2, 8, 4},
                        {2, 8, 7, 4, 1, 9, 6, 3, 5},
                        {3, 4, 5, 2, 8, 6, 1, 7, 1}
                };
            }
        };
    }

    @Test
    public void testCorrectness() throws Exception {
        this.evaluator.evaluateBoard(this.correctTestBoard);
        assertTrue(this.evaluator.isBoardCorrect());
    }

    @Test
    public void testIncorrectness() throws Exception {
        this.evaluator.evaluateBoard(this.incorrectTestBoard);
        assertFalse(this.evaluator.isBoardCorrect());
    }

    @Test
    public void testGetIncorrectRows() throws Exception {
        this.evaluator.evaluateBoard(this.incorrectTestBoard);
        assertTrue(this.evaluator.getIncorrectRows().size() == 1);
        assertTrue(this.evaluator.getIncorrectRows().get(0).equals(8));
    }

    @Test
    public void testGetIncorrectColumns() throws Exception {
        this.evaluator.evaluateBoard(this.incorrectTestBoard);
        assertTrue(this.evaluator.getIncorrectColumns().size() == 1);
        assertTrue(this.evaluator.getIncorrectColumns().get(0).equals(8));
    }

    @Test
    public void testGetIncorrectBlocks() throws Exception {
        this.evaluator.evaluateBoard(this.incorrectTestBoard);
        assertTrue(this.evaluator.getIncorrectBlocks().size() == 1);
        CellBlock incorrect = this.evaluator.getIncorrectBlocks().get(0);
        assertTrue(incorrect.getStartRow() == 6);
        assertTrue(incorrect.getStartColumn() == 6);
        assertTrue(incorrect.getEndRow() == 9);
        assertTrue(incorrect.getEndColumn() == 9);
    }
}
