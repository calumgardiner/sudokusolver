package io.calumg.sudoku.solver;

import io.calumg.sudoku.board.NineByNineSudokuBoard;
import io.calumg.sudoku.board.SudokuBoard;
import io.calumg.sudoku.evaluation.CellBlock;
import io.calumg.sudoku.evaluation.TwoDimensionalArrayUtils;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 9x9 Standard Sudoku board solver.
 * Created by calum on 09/05/2015.
 */
public class NineByNineSudokuSolver implements SudokuSolver {

    private SudokuBoard originalBoard;
    private SudokuBoard solvedBoard;
    private List<CellBlock> allCellBlocks;
    private CellPossibilities[][] boardPossibilities;
    private static final List<Integer> oneToNine = Arrays.asList(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9});

    public NineByNineSudokuSolver(SudokuBoard originalBoard) {
        if (!(originalBoard instanceof NineByNineSudokuBoard)) {
            throw new InvalidParameterException("This solver is for 9x9 standard boards only.");
        }
        this.originalBoard = originalBoard;
        this.solvedBoard = new NineByNineSudokuBoard();
        this.solvedBoard.setCells(originalBoard.getCells().clone());
    }

    public void solve() throws Exception {
        while (solveCell()) {
            // solving a cell per iteration
        }
    }

    /**
     * Do one iteration of trying to solve the cells. Checks where cell can only have 1 possibility. If board is
     * solvable then this should return true until completely solved.
     *
     * @return true if at least 1 cell was solved
     * @throws Exception
     */
    public Boolean solveCell() throws Exception {
        boardPossibilities = new CellPossibilities[9][9];
        Integer[][] cells = this.solvedBoard.getCells();
        Boolean cellSolved = false;
        // for each cell in the board
        for (int row = 0; row < cells.length; row++) {
            for (int column = 0; column < cells[0].length; column++) {
                Integer cell = cells[row][column];
                // if unsolved cell
                if (cell == null) {
                    // Create default cell possibilities 1-9
                    CellPossibilities cellPossibilities = new CellPossibilities();
                    for (Integer i : oneToNine) cellPossibilities.addPossibility(i);
                    // Get values from the block this cell is in already and remove them from the possibilities
                    CellBlock currentCellBlock = getCellBlockForCell(row, column);
                    for (Integer value : TwoDimensionalArrayUtils.getValuesInCellBlock(cells, currentCellBlock)) {
                        if (value != null) cellPossibilities.removePossibility(value);
                    }
                    // Remove values already in row
                    for (Integer value : TwoDimensionalArrayUtils.getNonNullObjectsInRow(cells, row)) {
                        cellPossibilities.removePossibility(value);
                    }
                    // Remove values already in column
                    for (Integer value : TwoDimensionalArrayUtils.getNonNullObjectsInColumn(cells, column)) {
                        cellPossibilities.removePossibility(value);
                    }
                    // Add the remaining possibilities to our holder
                    boardPossibilities[row][column] = cellPossibilities;
                    // If a cell only has 1 possibility we can assign it
                    if (cellPossibilities.getPossibilities().size() == 1) {
                        solvedBoard.setCell(row, column, cellPossibilities.getPossibilities().get(0));
                        cellSolved = true;
                    }
                }
            }
        }
        return cellSolved;
    }

    public CellPossibilities[][] lastIterationOfCellPossibilities() {
        return this.boardPossibilities;
    }

    public SudokuBoard getSolvedBoard() {
        return solvedBoard;
    }

    public SudokuBoard getUnsolvedBoard() {
        return this.originalBoard;
    }


    /**
     * Get the CellBlock for the specified cell.
     *
     * @param row
     * @param column
     * @return CellBlock for this cell
     * @throws Exception if cannot find a cell block for this cell
     */
    private CellBlock getCellBlockForCell(int row, int column) throws Exception {
        if (allCellBlocks == null) generateAllCellBlocks();
        for (CellBlock block : allCellBlocks) {
            if (row >= block.getStartRow() && row < block.getEndRow()
                    && column >= block.getStartColumn() && column < block.getEndColumn()) {
                return block;
            }
        }
        throw new Exception("Cannot find a block for " + row + "," + column);
    }

    /**
     * Generate all the cell blocks for a 9x9 board.
     */
    private void generateAllCellBlocks() {
        this.allCellBlocks = new ArrayList<CellBlock>();
        for (int row = 0; row < 9; row += 3) {
            for (int column = 0; column < 9; column += 3) {
                this.allCellBlocks.add(new CellBlock(row, row + 3, column, column + 3));
            }
        }
    }
}