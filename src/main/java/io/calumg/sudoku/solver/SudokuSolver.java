package io.calumg.sudoku.solver;

import io.calumg.sudoku.board.SudokuBoard;

/**
 * Created by calum on 09/05/2015.
 */
public interface SudokuSolver {

    /**
     * Solve the given unsolved board.
     */
    void solve() throws Exception;

    /**
     * Return the solved board.
     *
     * @return the solved board
     */
    SudokuBoard getSolvedBoard();

    /**
     * Get the original unsolved board.
     *
     * @return the originally given unsolved board
     */
    SudokuBoard getUnsolvedBoard();

    /**
     * Get the last iteration of cell possibilities before the solver finished.
     *
     * @return cell possibilities
     */
    CellPossibilities[][] lastIterationOfCellPossibilities();
}
