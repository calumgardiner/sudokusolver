package io.calumg.sudoku.solver;

import java.util.ArrayList;
import java.util.List;

/**
 * A list of the possibilities of a particular cell.
 * Created by calum on 09/05/2015.
 */
public class CellPossibilities {

    private List<Integer> possibilities;

    public CellPossibilities() {
        this.possibilities = new ArrayList<Integer>();
    }

    /**
     * Add a possibility to this cell.
     *
     * @param possibility
     */
    public void addPossibility(Integer possibility) {
        this.possibilities.add(possibility);
    }

    /**
     * Get all the possibilities to this cell.
     *
     * @return the possibilities
     */
    public List<Integer> getPossibilities() {
        return this.possibilities;
    }

    /**
     * Remove a possibility from this cell.
     *
     * @param possibility
     */
    public void removePossibility(Integer possibility) {
        this.possibilities.remove(possibility);
    }

}
