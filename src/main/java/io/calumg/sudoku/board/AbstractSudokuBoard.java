package io.calumg.sudoku.board;

import java.security.InvalidParameterException;

/**
 * Abstract implementation of the SudokuBoard provides functionality that most boards will use.
 * Created by calum on 09/05/2015.
 */
public abstract class AbstractSudokuBoard implements SudokuBoard {

    protected Integer x;
    protected Integer y;
    protected Integer[][] cells;

    public Integer[][] getCells() {
        return cells;
    }

    public void setCell(Integer row, Integer column, Integer value) throws InvalidParameterException {
        if (row >= this.x || row < 0) throw new InvalidParameterException("Row must be between 0 and " + this.x);
        if (column >= this.y || column < 0)
            throw new InvalidParameterException("Column must be between 0 and " + this.y);
        this.cells[row][column] = value;
    }


}
