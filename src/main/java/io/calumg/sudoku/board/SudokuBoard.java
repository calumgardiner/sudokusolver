package io.calumg.sudoku.board;

import org.omg.CORBA.DynAnyPackage.Invalid;

import java.security.InvalidParameterException;

/**
 * SudokuBoard interface describes any size of board.
 * Created by calum on 09/05/2015.
 */
public interface SudokuBoard {

    /**
     * Set the value of the cell at row,column
     *
     * @param row
     * @param column
     * @param value
     * @throws InvalidParameterException
     */
    void setCell(Integer row, Integer column, Integer value) throws InvalidParameterException;

    /**
     * Get the boards cells.
     *
     * @return the cells
     */
    Integer[][] getCells();

    void setCells(Integer[][] cells) throws InvalidParameterException;

}