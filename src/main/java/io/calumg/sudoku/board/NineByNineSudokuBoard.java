package io.calumg.sudoku.board;

import java.security.InvalidParameterException;

/**
 * 9 x 9 standard Sudouku board.
 * Created by calum on 09/05/2015.
 */
public class NineByNineSudokuBoard extends AbstractSudokuBoard {

    public NineByNineSudokuBoard() {
        this.x = 9;
        this.y = 9;
        this.cells = new Integer[x][y];
    }

    public void setCell(Integer row, Integer column, Integer value) throws InvalidParameterException {
        if (value > 9 || value < 1)
            throw new InvalidParameterException("The value must be 1-9");
        super.setCell(row, column, value);
    }

    public void setCells(Integer[][] cells) throws InvalidParameterException {
        if (cells.length != this.cells.length || cells[0].length != this.cells[0].length)
            throw new InvalidParameterException("You can only set a similar size of cells.");
        this.cells = cells;
    }
}


