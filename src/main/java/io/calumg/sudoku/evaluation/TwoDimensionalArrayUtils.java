package io.calumg.sudoku.evaluation;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for Integer arrays.
 * Created by calum on 09/05/2015.
 */
public class TwoDimensionalArrayUtils {

    /**
     * Sum the row of a 2D Integer array
     *
     * @param array
     * @param column
     * @return sum of the specified row
     */
    public static Integer sumOfColumn(Integer[][] array, Integer column) {
        Integer sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i][column];
        }
        return sum;
    }

    /**
     * Sum the column of a 2D Integer array.
     *
     * @param array
     * @param row
     * @return sum of the specified column
     */
    public static Integer sumOfRow(Integer[][] array, Integer row) {
        Integer sum = 0;
        for (int i = 0; i < array[0].length; i++) {
            sum += array[row][i];
        }
        return sum;
    }

    /**
     * Get all non-null elements in a particular row of a 2D array.
     *
     * @param cells
     * @param row
     * @param <T>
     * @return list containing the non-null values.
     */
    public static <T> List<T> getNonNullObjectsInRow(T[][] cells, Integer row) {
        List<T> objects = new ArrayList<T>();
        for (int i = 0; i < cells[0].length; i++) {
            T element = cells[row][i];
            if (element != null) objects.add(element);
        }
        return objects;
    }

    /**
     * Get all non-null elements in a particular column of a 2D array.
     *
     * @param cells
     * @param column
     * @param <T>
     * @return list containing the non-null values.
     */
    public static <T> List<T> getNonNullObjectsInColumn(T[][] cells, Integer column) {
        List<T> objects = new ArrayList<T>();
        for (int i = 0; i < cells.length; i++) {
            T element = cells[i][column];
            if (element != null) objects.add(element);
        }
        return objects;
    }

    /**
     * Generate a string of a 2D array in the format [cell.toString()] with newlines at end of each row.
     *
     * @param cells
     * @return String representation of a 2D array.
     */
    public static String stringOfArray(Object[][] cells) {
        String stringRepresentation = "";
        for (int row = 0; row < cells.length; row++) {
            for (int column = 0; column < cells[0].length; column++) {
                stringRepresentation += "[" + (cells[row][column] == null ? " " : cells[row][column]) + "]";
            }
            stringRepresentation += "\n";
        }
        return stringRepresentation;
    }


    /**
     * Checks whether the specified row in the 2d object array contains any duplicate objects.
     *
     * @param cells
     * @param row
     * @return true if there are duplicate objects in the specified row of the object array.
     */
    public static Boolean rowContainsDuplicates(Object[][] cells, Integer row) {
        List<Object> rowObjects = new ArrayList<Object>();
        for (int i = 0; i < cells[0].length; i++) {
            if (rowObjects.contains(cells[row][i])) return true;
            else rowObjects.add(cells[row][i]);
        }
        return false;
    }

    /**
     * Checks whether the specified column in the 2d object array contaains any duplicate objects.
     *
     * @param cells
     * @param column
     * @return true if there are duplicate objects in the specified column of the object array.
     */
    public static Boolean columnContainsDuplicates(Object[][] cells, Integer column) {
        List<Object> columnObjects = new ArrayList<Object>();
        for (int i = 0; i < cells.length; i++) {
            if (columnObjects.contains(cells[i][column])) return true;
            else columnObjects.add(cells[i][column]);
        }
        return false;
    }

    /**
     * Sample a subset of a 2d array into a list.
     *
     * @param cells       the cells to sample
     * @param startRow    row to start at
     * @param startColumn column to start at
     * @param rowSize     x size of the subset
     * @param columnSize  y size of the subset
     * @param <T>         type of 2d array
     * @return List of T of the subset.
     */
    public static <T> List<T> getSubsetOfArray(T[][] cells, Integer startRow, Integer startColumn,
                                               Integer rowSize, Integer columnSize) {
        List<T> subset = new ArrayList<T>();
        for (int row = startRow; row < startRow + rowSize; row++) {
            for (int column = startColumn; column < startColumn + columnSize; column++) {
                subset.add(cells[row][column]);
            }
        }
        return subset;
    }

    /**
     * Calls getSubsetOfArray for a CellBlock objects bounds.
     *
     * @param cells
     * @param block
     * @param <T>
     * @return List of T of the subset.
     * @see #getSubsetOfArray(Object[][], Integer, Integer, Integer, Integer)
     */
    public static <T> List<T> getValuesInCellBlock(T[][] cells, CellBlock block) {
        return getSubsetOfArray(cells, block.getStartRow(), block.getStartColumn(),
                (block.getEndRow() - block.getStartRow()), (block.getEndColumn() - block.getStartColumn()));
    }


}
