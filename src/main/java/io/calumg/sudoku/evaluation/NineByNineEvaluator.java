package io.calumg.sudoku.evaluation;

import io.calumg.sudoku.board.NineByNineSudokuBoard;
import io.calumg.sudoku.board.SudokuBoard;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

/**
 * io.calumg.sudoku.board.NineByNineSudokuBoard evaluator. Evaluates the correctness of the board and returns incorrect
 * rows and columns.
 * Created by calum on 09/05/2015.
 */
public class NineByNineEvaluator implements BoardEvaluator {

    private List<Integer> incorrectRows;
    private List<Integer> incorrectColumns;
    private Boolean isBoardCorrect;
    private List<CellBlock> incorrectCellBlocks;
    private static final Integer rowSum = 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9;

    public NineByNineEvaluator() {
        this.incorrectRows = new ArrayList<Integer>();
        this.incorrectColumns = new ArrayList<Integer>();
        this.incorrectCellBlocks = new ArrayList<CellBlock>();
        this.isBoardCorrect = Boolean.TRUE;
    }

    public void evaluateBoard(SudokuBoard board) throws InvalidParameterException {
        if (!(board instanceof NineByNineSudokuBoard))
            throw new InvalidParameterException("This evaluator will only calculate results for a 9x9 Board");
        reset();
        // Check all rows are valid
        for (int row = 0; row < 9; row++) {
            if (TwoDimensionalArrayUtils.rowContainsDuplicates(board.getCells(), row)) {
                incorrectRows.add(row);
                isBoardCorrect = false;
            }
        }
        // Check all columns are valid
        for (int column = 0; column < 9; column++) {
            if (TwoDimensionalArrayUtils.columnContainsDuplicates(board.getCells(), column)) {
                incorrectColumns.add(column);
                isBoardCorrect = false;
            }
        }
        // Check all 3x3 blocks
        Boolean blocksCorrect = noDuplicatesIn3by3Blocks(board.getCells());
        if (isBoardCorrect && !blocksCorrect) isBoardCorrect = false;
    }

    public Boolean isBoardCorrect() {
        return isBoardCorrect;
    }

    public List<Integer> getIncorrectRows() {
        return incorrectRows;
    }

    public List<Integer> getIncorrectColumns() {
        return incorrectColumns;
    }

    public List<CellBlock> getIncorrectBlocks() {
        return incorrectCellBlocks;
    }

    /**
     * Reset the internal state of the evaluator
     */
    private void reset() {
        this.incorrectRows.clear();
        this.incorrectColumns.clear();
        this.incorrectCellBlocks.clear();
        this.isBoardCorrect = Boolean.TRUE;
    }

    /**
     * Check cells for any 3x3 blocks with duplicates
     *
     * @param cells
     * @return
     */
    private Boolean noDuplicatesIn3by3Blocks(Integer[][] cells) {
        Boolean noDuplicates = true;
        for (int row = 0; row < 9; row += 3) {
            for (int column = 0; column < 9; column += 3) {
                Boolean check = noDuplicatesInBlock(cells, row, column);
                if (!check) {
                    noDuplicates = false;
                    incorrectCellBlocks.add(new CellBlock(row, row + 3, column, column + 3));
                }
            }
        }
        return noDuplicates;
    }

    /**
     * Checks whether the 3x3 block at row,cell has duplicates in it.
     *
     * @param cells
     * @param startRow
     * @param startColumn
     * @return true if duplicates exist else false
     */
    private Boolean noDuplicatesInBlock(Integer[][] cells, Integer startRow, Integer startColumn) {
        List<Integer> valuesInBlock = TwoDimensionalArrayUtils.getSubsetOfArray(cells, startRow, startColumn, 3, 3);
        List<Integer> checkedValues = new ArrayList<Integer>();
        for (Integer value : valuesInBlock) {
            if (checkedValues.contains(value)) return false;
            else checkedValues.add(value);
        }
        return true;
    }
}