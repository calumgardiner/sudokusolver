package io.calumg.sudoku.evaluation;

/**
 * A CellBlock, subset of the whole board, the start endRow and startColumn and end endRow and startColumn.
 * Created by calum on 09/05/2015.
 */
public class CellBlock {

    private Integer startColumn;
    private Integer endRow;
    private Integer endColumn;
    private Integer startRow;

    public CellBlock(Integer startRow, Integer endRow, Integer startColumn, Integer endColumn) {
        this.startRow = startRow;
        this.endRow = endRow;
        this.startColumn = startColumn;
        this.endColumn = endColumn;
    }

    /**
     * Get the start row.
     *
     * @return startRow
     */
    public Integer getStartRow() {
        return startRow;
    }

    /**
     * Set the start row.
     *
     * @param startRow
     */
    public void setStartRow(Integer startRow) {
        this.startRow = startRow;
    }

    /**
     * Get the end column.
     *
     * @return endColumn
     */
    public Integer getEndColumn() {
        return endColumn;
    }

    /**
     * Set the end column.
     *
     * @param endColumn
     */
    public void setEndColumn(Integer endColumn) {
        this.endColumn = endColumn;
    }

    /**
     * Get the start Column for this block.
     *
     * @return startColumn
     */
    public Integer getStartColumn() {
        return startColumn;
    }

    /**
     * Set the start Column for this block.
     *
     * @param startColumn
     */
    public void setStartColumn(Integer startColumn) {
        this.startColumn = startColumn;
    }

    /**
     * Get the endRow for this block.
     *
     * @return endRow
     */
    public Integer getEndRow() {
        return endRow;
    }

    /**
     * Set the endRow for this block.
     *
     * @param endRow
     */
    public void setEndRow(Integer endRow) {
        this.endRow = endRow;
    }

    @Override
    public String toString() {
        return "" + startRow + " : " + startColumn + " to " + endRow + " : " + endColumn;
    }
}
