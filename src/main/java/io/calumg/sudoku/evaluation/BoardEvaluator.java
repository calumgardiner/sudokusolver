package io.calumg.sudoku.evaluation;

import io.calumg.sudoku.board.SudokuBoard;

import java.security.InvalidParameterException;
import java.util.List;

/**
 * Interface for BoardEvaluators, each evaluator should be able to look at a board and tell whether it is correct or
 * not, should also be able to tell which rows and columns are incorrect.
 * Created by calum on 09/05/2015.
 */
public interface BoardEvaluator {

    /**
     * Evaluate the board and record the result internally. Might throw an exception if you try to evaluate
     * the wrong board.
     *
     * @param board
     * @throws InvalidParameterException
     */
    void evaluateBoard(SudokuBoard board) throws InvalidParameterException;

    /**
     * Check if the result is correct.
     *
     * @return boolean if board is correct.
     */
    Boolean isBoardCorrect();

    /**
     * Return any rows which were incorrect.
     *
     * @return the incorrect row references
     */
    List<Integer> getIncorrectRows();

    /**
     * Return any columns which were incorrect.
     *
     * @return the incorrect column references
     */
    List<Integer> getIncorrectColumns();

    /**
     * Return any blocks of cells which were incorrect.
     *
     * @return the incorrect cell blocks
     */
    List<CellBlock> getIncorrectBlocks();
}
